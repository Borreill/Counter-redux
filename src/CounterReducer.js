const CounterReducer = (state = 0, action) => {
  switch (action.type) {
      case 'ADD_1':
        return state + 1;
      case 'REMOVE_1':
        return state - 1;
      case 'RESET':
        return state = 0;
      case 'ADD_10':
        return state + 10;
      case 'REMOVE_10':
        return state - 10;
      default:
        return state;
  }
}

export default CounterReducer;